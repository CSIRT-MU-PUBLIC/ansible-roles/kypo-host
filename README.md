# Ansible role - KYPO-HOST

This role serves for basic KYPO host configuration.

Role supports Debian-like, RHEL-like and Kali OS.

## Requirements

This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

```yml
become: yes
```

## Role paramaters

* `kypo_dns` (optional) - List of extra DNS ip addresses. (default: ['147.251.4.33', '147.251.6.10'])

## Example

Example of simplest KYPO host installation:

```yml
roles:
    - role: kypo-host
      become: yes
```

## Maintainer notes

* The SSH keypair, that is used to communicate with user KYPO is located in `kypo2-deploy` repository in `csirt_mu/ssh.tar.gz` archive.